package com.stolbunov.roman.homework_29.mvp.presenter;

import android.util.Log;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.use_case.IUseCase;
import com.stolbunov.roman.homework_29.mvp.view.INoteListView;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class NoteListPresenter /*extends MvpPresenter<INoteListView>*/ {
    private final String ERROR_MESSAGE = "There was a problem loading the data. Try again.";
    private static String TAG = "TAG";
    private Disposable subscribe;

    private IUseCase useCase;
    private INoteListView view;

    @Inject
    NoteListPresenter(IUseCase useCase) {
        this.useCase = useCase;
    }

    public void setNoteListView(INoteListView view) {
        this.view = view;
    }

    public void showDialog() {
        view.showDialog();
    }

    public void hideDialog() {
        view.hideDialog();
    }

    public void loadDataFromDB() {
        Log.d("nik", "loadDataFromDB: ");
        view.showProgress();
        subscribe = useCase.getNoteList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::uploadSuccessful, err -> errorHandling(err, ERROR_MESSAGE));
    }

    public void add(Note note) {
        subscribe = useCase.add(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::operationSuccessful, this::errorHandling);
    }

    public void change(Note note) {
        subscribe = useCase.change(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::change, this::errorHandling);
    }

    public void remove(Note note) {
        if (useCase.remove(note)) {
            view.remove(note);
        } else {
            view.showErrorSaveNoteMessage(ERROR_MESSAGE);
        }
    }

    private void errorHandling(Throwable throwable, String message) {
        Log.d(TAG, throwable.getMessage());
        view.showErrorSaveNoteMessage(message);
    }

    private void errorHandling(Throwable throwable) {
        errorHandling(throwable, throwable.getMessage());
    }


    public void showErrorSaveMessage(String message) {
        view.showErrorSaveNoteMessage(message);
    }

    public void editNote(Note note) {
        view.goToEditor(note);
    }

    private void uploadSuccessful(List<Note> notes) {
        Log.d("nik", "uploadSuccessful: ");
        view.hideProgress();
        view.setDataAdapter(notes);
        subscribe.dispose();
    }

    private void operationSuccessful(Note note) {
        view.hideProgress();
        view.add(note);
        subscribe.dispose();
    }
}
