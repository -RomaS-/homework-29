package com.stolbunov.roman.homework_29.mvp.view;

import com.stolbunov.roman.domain.entity.Note;

public interface INoteEditorView {
    void showPriority(int priority);

    void editNote(Note note);
}
