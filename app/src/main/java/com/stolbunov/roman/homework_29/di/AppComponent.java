package com.stolbunov.roman.homework_29.di;

import android.content.Context;

import com.stolbunov.roman.homework_29.App;
import com.stolbunov.roman.homework_29.di.scope.AppScope;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

@AppScope
@Component(modules = {DaggerAndroidModule.class, DataModule.class})
public interface AppComponent extends AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder context(Context context);

        AndroidInjector<? extends DaggerApplication> build();
    }
}
