package com.stolbunov.roman.homework_29.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.stolbunov.roman.data.repository.IDBNote;
import com.stolbunov.roman.data.repository.RepositoryManager;
import com.stolbunov.roman.data.repository.data_source.DBNoteRepository;
import com.stolbunov.roman.domain.repository.INoteRepositoryBoundary;
import com.stolbunov.roman.domain.use_case.IUseCase;
import com.stolbunov.roman.domain.use_case.interactor.NoteListInteractor;
import com.stolbunov.roman.homework_29.R;
import com.stolbunov.roman.homework_29.di.scope.AppScope;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public interface DataModule {

    @AppScope
    @Binds
    INoteRepositoryBoundary provideRepositoryManager(RepositoryManager repositoryManager);

    @AppScope
    @Binds
    IDBNote provideDBNoteRepository(DBNoteRepository dbNoteRepository);

    @AppScope
    @Provides
    static SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences(context.getString(R.string.db_name), Context.MODE_PRIVATE);
    }

    @Binds
    IUseCase provideNoteListInteractor(NoteListInteractor noteListInteractor);
}
