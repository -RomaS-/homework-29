package com.stolbunov.roman.domain.repository;

import com.stolbunov.roman.domain.entity.Note;

import java.util.List;

import io.reactivex.Observable;

public interface INoteRepositoryBoundary {
    Observable add(Note note);

    boolean remove(Note note);

    Observable<Note> change(Note note);

    Observable<List<Note>> uploadNoteList();
}
