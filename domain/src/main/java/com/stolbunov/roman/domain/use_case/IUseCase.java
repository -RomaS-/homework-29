package com.stolbunov.roman.domain.use_case;

import com.stolbunov.roman.domain.entity.Note;

import java.util.List;

import io.reactivex.Observable;

public interface IUseCase {
    Observable<Note> add(Note note);

    boolean remove(Note note);

    Observable<Note> change(Note note);

    Observable<List<Note>> getNoteList();
}
