package com.stolbunov.roman.domain.use_case.interactor;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.repository.INoteRepositoryBoundary;
import com.stolbunov.roman.domain.use_case.BaseUseCase;
import com.stolbunov.roman.domain.use_case.IUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class NoteListInteractor extends BaseUseCase {
//    INoteRepositoryBoundary boundary;

    @Inject
    NoteListInteractor(INoteRepositoryBoundary boundary) {
        super(boundary);
    }

    @Override
    public Observable add(Note note) {
        return boundary.add(note);
    }

    @Override
    public boolean remove(Note note) {
        return boundary.remove(note);
    }

    @Override
    public Observable<Note> change(Note note) {
        return boundary.change(note);
    }

    @Override
    public Observable<List<Note>> getNoteList() {
        return boundary.uploadNoteList();
    }
}
