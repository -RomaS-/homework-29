package com.stolbunov.roman.data.entity;

public class LocaleNote {
    private long id;
    private String title;
    private String description;
    private String priority;

    public LocaleNote(String title, String description, String priority) {
        this(0, title, description, priority);
    }

    public LocaleNote(long id, String title, String description, String priority) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPriority() {
        return priority;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
