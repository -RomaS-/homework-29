package com.stolbunov.roman.data.executor;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.stolbunov.roman.data.entity.LocaleNote;

import java.util.LinkedList;
import java.util.List;

public class NotesLoadingExecutor extends AsyncTask<String, Void, List<LocaleNote>> {
    private long numberNotes;
    private SharedPreferences preferences;

    public NotesLoadingExecutor(long numberNotes, SharedPreferences preferences) {
        this.preferences = preferences;
        this.numberNotes = numberNotes;
    }

    @Override
    protected List<LocaleNote> doInBackground(String... strings) {
        List<LocaleNote> notes = new LinkedList<>();
        for (int i = 0; i < numberNotes; i++) {
            String title = preferences.getString(strings[0] + i, strings[0]);
            String description = preferences.getString(strings[1] + i, strings[1]);
            String priority = preferences.getString(strings[2] + i, strings[2]);

            if (!priority.equals(strings[2])) {
                notes.add(new LocaleNote(i, title, description, priority));
            }
        }
        return notes;
    }

    @Override
    protected void onPostExecute(List<LocaleNote> notes) {
        super.onPostExecute(notes);
    }
}
