package com.stolbunov.roman.data.mapper;

import com.stolbunov.roman.data.entity.LocaleNote;
import com.stolbunov.roman.domain.entity.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteMapper {
    public static LocaleNote transform(Note note) {
        return new LocaleNote(
                note.getId(),
                note.getTitle(),
                note.getDescription(),
                note.getPriorityToString());
    }

    public static Note transform(LocaleNote note) {
        return new Note(
                note.getId(),
                note.getTitle(),
                note.getDescription(),
                note.getPriority());
    }

    public static List<Note> transformList(List<LocaleNote> notes) {
        List<Note> list = new ArrayList<>(notes.size());
        for (LocaleNote localeNote : notes) {
            list.add(transform(localeNote));
        }
        return list;
    }
}
